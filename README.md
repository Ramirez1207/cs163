# Exercise 8
Brian Ramirez
CS 165
Exercise 8

1. I created an account on bitbucket.org

2. I created a new repository at Bitbucket

3. Using the command 'git clone https://Ramirez1207@bitbucket.org/Ramirez1207/cs163.git' I cloned the repository on my Linux machine.

4. I created this text file describing the steps I took to complete this assignment. I then staged, pushed, and commited this file to Bitbucket using the commands 'git add Exercise8' and 'git push'

5. I went to Gitbucket and edited the this text file I created and commited my change via Bitbucket web interface using the command 'git commit -m'

   Here is my edit

6. I issued the 'pull' command from within my repository directory on my Linux machine

7. I deleted this file

8. I used the 'checkout' command to restore this previously deleted file

9. I enabled access to the repository over SSH

10. I added yasinovskyy to your repository as a read-only user